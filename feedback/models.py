from django.conf import settings
from django.db import models
from django.db import models
from userProfile.models import User
from django.db.models.deletion import CASCADE
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.
class Feedback(models.Model):
    body = models.TextField()
    author = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)

    def __str__(self):
        return self.body
    
class Rate(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)
    nama = models.CharField(max_length=150, null=True)
    score = models.IntegerField(default = 0,
            validators = [
                MaxValueValidator(5),
                MinValueValidator(0),
            ]
            )
    def __str__(self):
        return self.author.username