from django.urls import path

from . import views

app_name = 'feedback'

urlpatterns = [
    path('', views.home, name='feedback'),
    path('add/', views.home, name = 'add'),
]