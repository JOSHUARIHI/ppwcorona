from django.test import TestCase,Client
from django.http.request import HttpRequest
from django.urls import reverse, resolve
from django.contrib.auth import get_user_model
from django.contrib.auth import login, authenticate, logout

# Create your tests here.
from .models import Feedback, User, Rate
from . import views
from userProfile.models import Account


# Create your tests here.
class Story6(TestCase):
    
    def test_home_page(self):
        response = Client().get('/feedback/')
        self.assertEqual(response.status_code, 200)
        
    def test_template_home(self):
        response = Client().get('/feedback/')
        self.assertTemplateUsed(response, 'feedback.html')
        
    def test_func_home(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, views.home)
        
    def test_model_kegiatan(self):
        penulis = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        Feedback.objects.create(
            body='HALO',
            author = penulis,
        )
        hasil = Feedback.objects.get(body='HALO')
        self.assertEqual(str(hasil), 'HALO')
    
    def test_story6_jumlah_model_kegiatan(self):
        penulis1 = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        Feedback.objects.create(body='HALO', author = penulis1)
        penulis2 = User.objects.create_user('syay', 'lennon@thebeatles.com', 'johnpassword')
        Feedback.objects.create(body="hai", author = penulis2)
        penulis3 = User.objects.create_user('dia', 'lennon@thebeatles.com', 'johnpassword')
        Feedback.objects.create(body="dia", author = penulis3)
        penulis4 = User.objects.create_user('kamu', 'lennon@thebeatles.com', 'johnpassword')
        Feedback.objects.create(body="baik", author = penulis4)
        penulis5 = User.objects.create_user('kita', 'lennon@thebeatles.com', 'johnpassword')
        Feedback.objects.create(body="saya", author = penulis5)
        jumlah = Feedback.objects.all().count()
        self.assertEqual(jumlah, 5)
        
    def test_try_add_feedback_before_login(self):
        response = self.client.get(reverse('feedback:feedback'))
        self.assertContains(response, "You must be logged in before posting a feedback")

    def dummy_user(self):
        user = User.objects.create(username="dummy", email="dummy@example.com")
        akun = Account.objects.create(
            user = user,
			name = user.username,
			email = user.email,
			job = "petani",
			age = 15,
        )
        user.set_password("12345")
        user.save()

        self.client.login(
            username="dummy",
            password="12345"
        )
        
    def test_add_feedback_get(self):
        self.dummy_user();
        response = self.client.get(reverse('feedback:feedback'))
        self.assertTemplateUsed(response, 'feedback.html')
        
    def test_add_feedback_post(self):
        self.dummy_user();
        response = self.client.post(
            reverse('feedback:feedback'),
            data= {'body' : 'haiii papa kabar'}
            )
        self.assertEqual(Feedback.objects.all().count(), 1)
        self.assertEqual(response.status_code, 200)
        response =  Feedback.objects.get(body='haiii papa kabar')
        self.assertEqual(str(response), "haiii papa kabar")

        
    def test_rate(self):
        response = Client().get('/rate/')
        self.assertEqual(response.status_code, 200)
        
    def test_rate_func(self):
        response = resolve('/rate/')
        self.assertEqual(response.func, views.olah)
        

