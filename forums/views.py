from django.http.request import HttpRequest
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from .models import Forum
from .forms import ForumForm, CommentForm

def forums(request, warn=False):
    request.session['forums_item'] = Forum.objects.all().count()
    context = {
        'forums'    : Forum.objects.all(),
        'warn'      : warn
    }
    return render(request, 'forums/forums.html', context)

def add_forum(request):
    if not request.user.is_authenticated:
        return forums(request, warn=True)

    form = ForumForm()
    if request.method == "POST":
        form = ForumForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.author = request.user.account
            instance.save()
            return redirect('forums:forums')

    context = {
        "form"      : form
    }
    return render(request, 'forums/add_forum.html', context)

def detail_forum(request, pk):
    forum = Forum.objects.get(id=pk)
    comment_form = CommentForm()
    context = {
        "forum"  : forum,
        "author" : True,
        "warn" : False
    }

    if request.user != forum.author.user:
        context['author'] = False

    if not request.user.is_authenticated:
        context['warn'] = True
        return render(request, 'forums/detail_forum.html', context)

    if request.method == "POST":
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            instance = comment_form.save(commit=False)
            instance.author = request.user.account
            instance.save()
            return redirect('forums:forums_detail_forum', pk)
    
    context['comment_form'] = comment_form
    return render(request, 'forums/detail_forum.html', context)

def delete_forum(request, pk):
    forum = Forum.objects.get(id=pk)
    if request.user != forum.author.user:
        return redirect('forums:forums_detail_forum', pk)
    if request.method == 'POST':
        forum.delete()
    return redirect('forums:forums')