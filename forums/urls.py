from django.urls import path

from . import views

app_name = 'forums'

urlpatterns = [
    path('', views.forums, name='forums'),
    path('add_forum/', views.add_forum, name='forums_add_forum'),
    path('<int:pk>', views.detail_forum, name='forums_detail_forum'),
    path('delete_forum/<int:pk>', views.delete_forum, name='forums_delete_forum'),
]