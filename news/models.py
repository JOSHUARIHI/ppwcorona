from corona_app.settings import TIME_ZONE
from django.db import models
from django.utils import timezone
import pytz
# Create your models here.

# News
# - title (API attr)
# - description (API attr)
# - content (lebih panjang dari description) (API attr)
# - source (API attr)
# - author (API attr)
# - timestamp (API attr)


class News(models.Model):
    title = models.CharField(max_length=200, null=True)
    name = models.CharField(max_length=200, null=True)
    author = models.CharField(max_length=100, null=True)

    description = models.CharField(max_length=400, null=True)
    content = models.CharField(max_length=400, null=True)
    source = models.CharField(max_length=400, null=True)
    image_url = models.CharField(max_length=300, blank=True, null=True)
    timestamp = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ['-timestamp']

    def __str__(self):
        return self.title
