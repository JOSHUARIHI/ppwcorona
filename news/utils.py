from .models import News

import requests
from corona_app.settings import API_KEY
from requests.exceptions import ConnectionError

from dateutil import parser


def retrieve_API():
    url = 'http://newsapi.org/v2/top-headlines?q=covid&country=id&category=health&apiKey='+API_KEY
    try:
        response = requests.get(url)
    except ConnectionError:
        response = {}
    
    try:
        list_articles = response.json()['articles']
        for news in list_articles:
            try:
                if news['title'] and news['description'] and news['content'] and news['urlToImage'] != None:
                    
                    # response = requests.get(news['urlToImage'])
                    image_url = news['urlToImage']
                    # print(response)
                    # if response.status_code != 200:
                    #     image_url = '/static/news/news_default.svg'
                    
                    obj, created = News.objects.update_or_create(
                        title=news['title'],
                        name=news['source']['name'],
                        author=news['author'],
                        description=news['description'],
                        content=news['content'],
                        source=news['url'],
                        image_url=image_url,
                        timestamp=parser.isoparse(news['publishedAt'])
                    )
            except News.MultipleObjectsReturned:
                News.objects.filter(\
                    pk__in=News.objects\
                        .filter(title=news['title'])\
                        .values_list('pk')[1:]).delete()
            except:
                continue
    except KeyError as e:
        pass
