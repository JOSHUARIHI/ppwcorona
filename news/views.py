from .models import News
from django.shortcuts import render

# Create your views here.
from .utils import retrieve_API

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def news(request):

    retrieve_API()
    articles = News.objects.all().order_by('-timestamp')
    paginator = Paginator(articles, 10)
    q_page = request.GET.get('page')
    page = int(q_page) if q_page!=None else None

    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page(1)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    context = {
        'articles': articles,
        'page': page,
        'paginator': paginator
    }
    return render(request, 'news/news.html', context)

def news_detail(request, news_id, news_title):
    obj = News.objects.get(pk=news_id)
    context = {
        'news': obj
    }
    return render(request, 'news/news_detail.html', context)