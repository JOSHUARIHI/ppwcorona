from django.urls import path

from . import views

app_name = 'news'

urlpatterns = [
    path('', views.news, name='news'),
    path('<int:news_id>/<str:news_title>/', views.news_detail, name='news_detail'),
]