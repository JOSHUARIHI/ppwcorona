from datetime import datetime
from logging import DEBUG
from django.http.request import HttpRequest
from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.utils.dateparse import parse_datetime
from .views import *
from .models import *
from .apps import NewsConfig
from corona_app.settings import API_KEY
from .utils import retrieve_API


# Create your tests here.


class TestNews(TestCase):

    def setUp(self):
        News.objects.create(
            title='berita baru'
        )

    def test_apps(self):
        self.assertEqual(NewsConfig.name, 'news')

    def test_news_detail_view(self):
        response = self.client.get(
            reverse('news:news_detail', args=[1,'berita baru'])
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(News.objects.all().count(), 1)

    def test_news_fetch_API(self):
        response = self.client.get(
            reverse('news:news')
        )
        self.assertEqual(response.status_code, 200)

    def test_models_toString(self):
        self.assertEqual(str(News.objects.get(id=1)), "berita baru")