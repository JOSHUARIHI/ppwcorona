from django.urls import path, include

from . import views

app_name = 'APP1'

urlpatterns = [
    path('', views.index, name='index'),
    path('search/', views.news_search_result, name='search'),
]