from django.http.request import HttpRequest
from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import *
from news.models import *
from news.utils import *
from .apps import App1Config

# Create your tests here.


class TestApp(TestCase):

    def test_url_landing_page(self):
        response = self.client.get(reverse('APP1:index'))
        self.assertEqual(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(App1Config.name, 'APP1')

    def test_search_bar_result(self):
        retrieve_API()
        response = self.client.get(reverse('APP1:search'), {
            'search': 'virus'
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('virus', response.content.decode('utf8'))

        response = self.client.post(reverse('APP1:search'))
        self.assertEqual(response.status_code, 302)
    # def test_search_result(self):
    #     url = self.client.get(
    #         reverse('APP1:search'),
    #         {'search': 'anak'}
    #     )
    #     self.assertEqual(url.resolver_match.func, news_search_result)
    #     print(url.context)
        # request = HttpRequest()
        # print(request.GET.get('search'))
