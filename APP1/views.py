from django.shortcuts import redirect, render
from news.models import News
from forums.models import Forum
from django.db.models import Q
from news.utils import retrieve_API
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# from forum.models import Forum
# Create your views here.


def index(request):
    retrieve_API()
    request.session['forums_item'] = Forum.objects.all().count()
    request.session['news_item'] = News.objects.all().count()
    context = {
        'list_news': News.objects.all().order_by('-timestamp')[:3]
    }
    return render(request, 'APP1/landing.html', context)

def news_search_result(request):
    
    if request.method == "GET":
        keyword = request.GET.get('search')
        articles = News.objects\
            .filter(Q(title__icontains=keyword) | Q(content__icontains=keyword))\
            .order_by('-timestamp')
        paginator = Paginator(articles, 10)
        q_page = request.GET.get('page')
        page = int(q_page) if q_page != None else None

        try:
            articles = paginator.page(page)
        except PageNotAnInteger:
            articles = paginator.page(1)
        except EmptyPage:
            articles = paginator.page(paginator.num_pages)

        context = {
            'articles': articles,
            'page': page,
            'paginator': paginator,
            'key':keyword
        }
        return render(request, 'APP1/search_result.html', context)
    return redirect('APP1:index')